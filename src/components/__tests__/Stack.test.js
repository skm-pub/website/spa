import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import Stack from '../Stack.vue'

describe('Stack', () => {
  it('renders properly', () => {
    const wrapper = mount(Stack)
    expect(wrapper.text()).toContain('Front')
  })
})
